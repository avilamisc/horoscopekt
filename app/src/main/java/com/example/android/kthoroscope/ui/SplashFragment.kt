package com.example.android.kthoroscope.ui


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.android.kthoroscope.R
import com.example.android.kthoroscope.utils.toggleVisibility
import kotlinx.android.synthetic.main.activity_main.*


class SplashFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // hide bottom navigation view
        activity?.bottom_nav_view?.toggleVisibility()

        Handler().postDelayed({
            Navigation
                .findNavController(view)
                .navigate(SplashFragmentDirections.actionSplashFragmentToDestinationHoroscope())
        }, 1000)


    }

}
