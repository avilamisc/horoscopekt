package com.example.android.kthoroscope.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.android.kthoroscope.utils.toggleVisibility
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_tarot_card_detail.*

class TarotCardDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        activity?.bottom_nav_view?.toggleVisibility()
        return inflater.inflate(com.example.android.kthoroscope.R.layout.fragment_tarot_card_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCardDetailsFromArgs()
        detail_toolbar.setNavigationOnClickListener { Navigation.findNavController(it).popBackStack() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.bottom_nav_view?.toggleVisibility()
    }

    private fun setCardDetailsFromArgs() {
        val card = arguments?.let { TarotCardDetailFragmentArgs.fromBundle(it).detailsOfCard }
        if (card != null) {
            iv_detail_card.setImageDrawable(context?.getDrawable(card.imageId))
            tv_detail_title.text = card.title
            tv_detail_description.text = card.description
        }
    }

}
