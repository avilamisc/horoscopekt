package com.example.android.kthoroscope.viewmodels

import android.app.Application
import android.view.View
import androidx.core.view.isGone
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.example.android.kthoroscope.R
import com.example.android.kthoroscope.data.TarotCard
import com.example.android.kthoroscope.ui.TarotFragmentDirections
import kotlinx.android.synthetic.main.fragment_tarot.view.*
import kotlinx.android.synthetic.main.item_tarot_card.view.*

class TarotViewModel(application: Application) : AndroidViewModel(application) {

    /* Full list of the cards */
    private val listOfCards = listOf(
        TarotCard("The Chariot", application.getString(R.string.text_card_chariot_1), R.drawable.card_chariot),
        TarotCard("The Death", application.getString(R.string.text_card_death_1), R.drawable.card_death),
        TarotCard("The Devil", application.getString(R.string.text_card_devil_1), R.drawable.card_devil),
        TarotCard("The Emperor", application.getString(R.string.text_card_emperor_1), R.drawable.card_emperor),
        TarotCard("The Empress", application.getString(R.string.text_card_empress_1), R.drawable.card_empress),
        TarotCard("The Fool", application.getString(R.string.text_card_fool_1), R.drawable.card_fool),
        TarotCard("The Hermit", application.getString(R.string.text_card_hermit_1), R.drawable.card_hermit),
        TarotCard("The Judgement", application.getString(R.string.text_card_judgement_1), R.drawable.card_judgement),
        TarotCard("The Justice", application.getString(R.string.text_card_justice_1), R.drawable.card_justice),
        TarotCard("The Lovers", application.getString(R.string.text_card_lovers_1), R.drawable.card_lovers),
        TarotCard("The Magician", application.getString(R.string.text_card_magician_mean_1), R.drawable.card_magician),
        TarotCard("The Moon", application.getString(R.string.text_card_moon_1), R.drawable.card_moon),
        TarotCard("The Pope", application.getString(R.string.text_card_pope_1), R.drawable.card_pope),
        TarotCard("The Priestess", application.getString(R.string.text_card_priestess_mean_1), R.drawable.card_priestess),
        TarotCard("The Star", application.getString(R.string.text_card_star_1), R.drawable.card_star),
        TarotCard("The Strength", application.getString(R.string.text_card_strength_1), R.drawable.card_strength),
        TarotCard("The Sun", application.getString(R.string.text_card_sun_1), R.drawable.card_sun),
        TarotCard("The Temperance", application.getString(R.string.text_card_temperance_1), R.drawable.card_temperance),
        TarotCard("The Tower", application.getString(R.string.text_card_tower_1), R.drawable.card_tower),
        TarotCard("The Wheel Fortune", application.getString(R.string.text_card_wheel_fortune_1), R.drawable.card_wheel_fortune),
        TarotCard("The World", application.getString(R.string.text_card_world_1), R.drawable.card_world),
        TarotCard("The Hanged Man", application.getString(R.string.text_card_hanged_man_1), R.drawable.card_hanged_man)
    )
    private val randomListOfCards = arrayListOf<TarotCard>()

    private var mSelectedItemPosition: Int = -1

    private lateinit var firstCard: TarotCard
    private lateinit var secondCard: TarotCard
    private lateinit var thirdCard: TarotCard
    private var threeCardsChosen: Boolean = false


    init {
        shuffleListOfCards()
    }

    private fun shuffleListOfCards() {
        while (randomListOfCards.size < 7) {
            val randomCard = listOfCards.random()
            if (!randomListOfCards.contains(randomCard)) randomListOfCards.add(randomCard)
        }
    }

    fun getDeckOfCards(): ArrayList<TarotCard> = randomListOfCards

    fun handleClickCard(view: View, position: Int) {
        run {
            val isCardSelected = view.tarot_card_empty_view.isGone
            if (isCardSelected) view.tarot_card_empty_view.visibility = View.VISIBLE
            else view.tarot_card_empty_view.visibility = View.GONE
            mSelectedItemPosition = position
        }
    }

    fun setCard(card: TarotCard, number: Int) {
        when (number) {
            1 -> firstCard = card
            2 -> secondCard = card
            3 -> {
                thirdCard = card
                threeCardsChosen = true
            }
        }
    }

    fun getCard(number: Int) = when(number) {
        1 -> firstCard
        2 -> secondCard
        else -> thirdCard
    }

    fun isThreeCardsChosen() = threeCardsChosen

    fun handleOpenDetailsClick(view: View) {
        if (threeCardsChosen) {
            val action: NavDirections = when (view) {
                view.iv_first_card -> TarotFragmentDirections.actionDestinationTarotToTarotCardDetailFragment(firstCard)
                view.iv_second_card -> TarotFragmentDirections.actionDestinationTarotToTarotCardDetailFragment(secondCard)
                else -> TarotFragmentDirections.actionDestinationTarotToTarotCardDetailFragment(thirdCard)
            }
            Navigation.findNavController(view).navigate(action)
        }
    }


}