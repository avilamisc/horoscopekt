package com.example.android.kthoroscope.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TarotCard(
    val title: String,
    val description: String,
    val imageId: Int
) : Parcelable