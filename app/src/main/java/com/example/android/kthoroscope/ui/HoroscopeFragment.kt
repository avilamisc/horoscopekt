package com.example.android.kthoroscope.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.android.kthoroscope.R
import com.example.android.kthoroscope.utils.toggleVisibility
import kotlinx.android.synthetic.main.activity_main.*

class HoroscopeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // todo: disable full screen mode
        // return visibility of the bottom nav view
        activity?.bottom_nav_view?.toggleVisibility()
        return inflater.inflate(R.layout.fragment_horoscope, container, false)
    }

}
