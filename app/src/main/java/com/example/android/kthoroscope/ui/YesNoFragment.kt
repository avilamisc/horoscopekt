package com.example.android.kthoroscope.ui

import android.content.Context
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import com.example.android.kthoroscope.R
import com.squareup.seismic.ShakeDetector
import kotlinx.android.synthetic.main.fragment_yes_no.*


class YesNoFragment : Fragment(), ShakeDetector.Listener {
    private lateinit var sensorManager: SensorManager
    private val shakeDetector = ShakeDetector(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.android.kthoroscope.R.layout.fragment_yes_no, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_ball.setOnClickListener { hearShake() }
    }

    override fun hearShake() {
        // show the shake animation
        val shakeAnim = AnimationUtils.loadAnimation(context, R.anim.anim_shake)
        shakeAnim.setAnimationListener(object: Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {}

            override fun onAnimationEnd(animation: Animation?) {
                // show an answer
                val answers = listOf("YES", "MAY BE", "NO")
                val random = (0 until answers.size).random()
                tv_ball_answer.text = answers[random]
                tv_ball_answer.visibility = View.VISIBLE
            }

            override fun onAnimationStart(animation: Animation?) { tv_ball_answer.visibility = View.INVISIBLE }
        })
        iv_ball.startAnimation(shakeAnim)
    }


    override fun onResume() {
        super.onResume()
        sensorManager = context?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        shakeDetector.setSensitivity(ShakeDetector.SENSITIVITY_LIGHT)
        shakeDetector.start(sensorManager)
    }

    override fun onStop() {
        super.onStop()
        shakeDetector.stop()
        sensorManager.unregisterListener(shakeDetector)
    }

}
