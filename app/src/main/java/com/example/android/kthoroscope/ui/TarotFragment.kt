package com.example.android.kthoroscope.ui


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.android.kthoroscope.ItemDecoration
import com.example.android.kthoroscope.adapter.TarotAdapter
import com.example.android.kthoroscope.data.TarotCard
import com.example.android.kthoroscope.viewmodels.TarotViewModel
import kotlinx.android.synthetic.main.fragment_tarot.*


class TarotFragment : Fragment() {

    private lateinit var viewModel: TarotViewModel
    private lateinit var mAdapter: TarotAdapter

    private val TAG = "TarotFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TarotViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.android.kthoroscope.R.layout.fragment_tarot, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated()")
        setupRecyclerView()
        setClickListeners()

        recreateView()
    }

    /*  When return back from the DetailTarotFragment (for recreating view) */
    private fun recreateView() {
        if (viewModel.isThreeCardsChosen()) {
            iv_first_card.setImageDrawable(context?.getDrawable(viewModel.getCard(1).imageId))
            iv_second_card.setImageDrawable(context?.getDrawable(viewModel.getCard(2).imageId))
            iv_third_card.setImageDrawable(context?.getDrawable(viewModel.getCard(3).imageId))
            tarot_recycler.visibility = View.INVISIBLE
        }
    }

    private fun setClickListeners() {
        iv_first_card.setOnClickListener {
            viewModel.handleOpenDetailsClick(it)
        }
        iv_second_card.setOnClickListener {
            viewModel.handleOpenDetailsClick(it)
        }
        iv_third_card.setOnClickListener {
            viewModel.handleOpenDetailsClick(it)
        }
    }

    private fun setupRecyclerView() {
        mAdapter = TarotAdapter(viewModel.getDeckOfCards()) { view: View, position: Int, _: TarotCard ->
            viewModel.handleClickCard(view, position)
        }

        tarot_recycler?.apply {
            setHasFixedSize(true)
            adapter = mAdapter
            addItemDecoration(ItemDecoration(-100))
            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.UP) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean = true


                override fun onSwiped(holder: RecyclerView.ViewHolder, direction: Int) {
                    val position = holder.adapterPosition
                    val card = viewModel.getDeckOfCards()[position]
                    val drawable = context.getDrawable(card.imageId)

                    when {
                        iv_first_card.drawable == null -> {
                            iv_first_card.setImageDrawable(drawable)
                            viewModel.setCard(card, 1)
                        }
                        iv_second_card.drawable == null -> {
                            iv_second_card.setImageDrawable(drawable)
                            viewModel.setCard(card, 2)
                        }
                        iv_third_card.drawable == null -> {
                            iv_third_card.setImageDrawable(drawable)
                            viewModel.setCard(card, 3)
                        }
                    }
                    mAdapter.removeCard(position)

                    if (viewModel.isThreeCardsChosen()) tarot_recycler.visibility = View.INVISIBLE

                }

            }).attachToRecyclerView(this)
        }
    }
}
